using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHandlerTest : MonoBehaviour
{
    bool picked = false;
    Transform pickedLastParent;

    public GameObject pickedPosition;

    List<GameObject> allObjTrigger;

    BoxCollider col;
    public GameObject box;

    OVRInput.Button inputButton = OVRInput.Button.SecondaryHandTrigger;

    void Start()
    {
        allObjTrigger = new List<GameObject>();
        col = GetComponent<BoxCollider>();
        box.transform.localScale = col.size;
    }

    void Update()
    {
        if (OVRInput.GetDown(inputButton))
        {
            GameObject pickObj = allObjTrigger[0];
            if (pickObj != null)
            {
               // if (picked)
               // {
               //     picked = false;
               //     pickObj.transform.SetParent(pickedLastParent);
               //     if (allObjTrigger.Contains(pickObj))
               //         allObjTrigger.Remove(pickObj);
               // }
               // else
               // {
               //     picked = true;
                    pickedLastParent = pickObj.transform.parent;
                    pickObj.transform.parent = pickedPosition.transform;
                    //pickObj.transform.localPosition = Vector3.zero;
                    //pickObj.transform.localRotation = Quaternion.identity;
               // }
            }
        }
        if (OVRInput.GetUp(inputButton))
        {
            GameObject pickObj = allObjTrigger[0];
            if (pickObj != null)
            {
                pickObj.transform.SetParent(pickedLastParent);
                if (allObjTrigger.Contains(pickObj))
                    allObjTrigger.Remove(pickObj);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pickable"))
            if (!allObjTrigger.Contains(other.gameObject))
                allObjTrigger.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pickable"))
            if (allObjTrigger.Contains(other.gameObject))
                allObjTrigger.Remove(other.gameObject);
    }
}
